99999999 PRINT "What is your name: ";

999999999 INPUT name$

9999999999 PRINT "Welcome to Dungeon Master, " + name$ + "!"

11 PRINT "One day you were exploring a series"

12 PRINT "of dungeons you had found, when"

13 PRINT "suddently a monster appears!"

4 PRINT "What will you do?"

PRINT "Type fight, attack, nothing, defend or run."

5 INPUT c$

6 IF c$ = "fight" THEN

    GOTO 700

15 END IF

32 IF c$ = "attack" THEN

    GOTO 700

33 END IF

34 IF c$ = "nothing" THEN

    GOTO 36

35 END IF

40 IF c$ = "defend" THEN

    GOTO 41

39 END IF

7 PRINT "You ran away"

8 GOTO 24

700 PRINT "You defeated the monster!"

9 GOTO 51

101 PRINT "You have been defeated, GAME OVER"

10 PRINT "END OF BETA,"

38 PRINT "Thanks  for playing, " + name$ + "!"

16 GOTO 2

51 PRINT

17 PRINT

18 PRINT "but now you are badly wounded and chances of"

19 PRINT "making it to the surface are slim. You"

PRINT "realize you can either attempt to make it"

21 PRINT "to the surface where you can get healing"

22 PRINT "from your house OR continue down"

PRINT "deeper into the dungeons..."

58 PRINT

59 PRINT "Type house or dungeon"

60 INPUT j$

61 IF j$ = "house" THEN

    GOTO 63

62 END IF

64 IF j$ = "dungeon" THEN

    GOTO 126

65 END IF

23 GOTO 59

24 PRINT "You have run and you are now lost and can't"

25 PRINT "find the way back. You continue wondering"

26 PRINT "aimlessly around the dungeons when you"

27 PRINT "find you are surrounded by monsters!"

28 PRINT "Theres no way out and you know you can't"

29 PRINT "possibly win but you still pull out your sword"

30 PRINT "and holler 'COME AND GET SOME!'"

31 PRINT "You defeat 3 smaller monsters before they overwhelm you."

100 GOTO 101

36 PRINT "You stand still as the monster strikes you down."

GOTO 101

41 PRINT "You hold your ground and put your sheild up,"

PRINT "the monster hits it relentlessly but to no avail."

43 PRINT "Eventually the monster gives up and stomps off to find other prey."

44 PRINT "You cautiously lower your sheild and"

45 PRINT "inspect for damage. Your sheild has"

46 PRINT "a deep gounge in it and you will not be able"

47 PRINT "to use it before you mend it. There are"

48 PRINT "two options from here, go to the surface and"

49 PRINT "mend your sheild using 3 of the 12 iron you have,"

50 PRINT "OR adventure deeper into the dungeons."

52 PRINT

53 PRINT "Type dungeon OR surface"

102 INPUT s$

103 IF s$ = "dungeon" THEN

    GOTO 105

104 END IF

106 IF s$ = "surface" THEN

    GOTO 108

107 END IF

42 GOTO 102

105 PRINT "coming soon!"

109 GOTO 10

108 PRINT "coming soon!"

110 GOTO 10

63 PRINT "You start heading in the direction back"

112 PRINT "to your house but the loss of blood is too much"

113 PRINT "and you start getting dizzy. You fall on"

114 PRINT "the floor and slowly bleed to death."

111 GOTO 101

126 PRINT

66 PRINT "You decide to go down farther but you start"

67 PRINT "feeling dizzy, you realize you need to stop"

68 PRINT "the flow of blood, but with what?"

69 PRINT "Then you come across a part where the"

70 PRINT "cave brances off to the left and the right. You"

71 PRINT "realize you need to choose between the two"

72 PRINT "and either one could mean life or death."

73 PRINT

74 PRINT "Type left to take the left cave and"

128 PRINT "right for the right"

75 INPUT y$

76 IF y$ = "left" THEN

    GOTO 80

77 END IF

78 IF y$ = "right" THEN

    GOTO 81

79 END IF

82 GOTO 74

80 PRINT "You go left and as you walk through you see"

85 PRINT "beside the door a loose stray of cloth hanging"

86 PRINT "from a nail. You snatch it up and quickly"

94 PRINT "tie it around your torso. The white wool"

95 PRINT "stains red and it stops the blood flow."

96 PRINT "You let out a sigh of relief but you are now faced"

97 PRINT "with a differant descision, go back up to the"

98 PRINT "surface, or continue going deeper?"

99 PRINT

115 PRINT "Type deeper to go deeper and surface to"

116 PRINT "go to the surface"

117 INPUT f$

118 IF f$ = "deeper" THEN

    GOTO 120

119 END IF

121 IF f$ = "surface" THEN

    GOTO 123

122 END IF

84 GOTO 115

81 PRINT "You step through the mouth of the cave"

130 PRINT "on the right"

87 PRINT "and as you're looking"

131 PRINT "inside the small room an"

88 PRINT "animal above cause a"

132 PRINT "minature avalance to block"

89 PRINT "the doorway. You try to move the rocks but"

90 PRINT "your loss if blood is making you weak."

91 PRINT "the stress of trying to move the rocks with"

92 PRINT "low blood makes you faint and as you lay"

93 PRINT "there the blood slowly seeps out of you."

83 GOTO 101

120 PRINT "Coming Soon!"

124 GOTO 10

123 PRINT

PRINT "You decide its safe to go to the house, but"

127 PRINT "as you make your way up you encounter"

PRINT "another monster, as you draw your sword,"

129 PRINT "the monter runs into you knocking you back and"

PRINT "sending your sword and sheild flying in"

PRINT "opposite directions. As the monster starts"

PRINT "getting up you realize you have two choices,"

PRINT "go for your sword, which is farther away,"

PRINT "or your sheild, which is closer."

PRINT 5

PRINT "Type sword or sheild"

133 INPUT sworsh$

134 IF sworsh$ = "sword" THEN

    GOTO 135

136 END IF

'sheild is correct

137 IF sworsh$ = "sheild" THEN

    GOTO 138

139 END IF

GOTO 132

135 PRINT "You make a mad dash for your sword but when"

PRINT "you almost have it the monster rams into you"

PRINT "from the side and sends you flying. The broken"

PRINT "rib bones puncture the skin and you start bleeding like crazy."

PRINT "The last thing you see is your sword, just inches out of your grip."

PRINT

GOTO 101

138 PRINT "You do a dive roll and pick up your sheild"

PRINT "just in time, you hold it sideways so as the monster"

PRINT "runs into it, it just bounces off into the wall behind."

PRINT "As the monster strugles to free it's head from the wall"

PRINT "you grab your sword and free it's head from its shoulders."

PRINT "The monster explodes into a smaller red coloured soul stone"

PRINT "that you pick up and put in your pouch. You now have this"

PRINT "stone and the auburn color stone you got from the first"

PRINT "monster you saw."
GOTO 10

