_TITLE "Files Test"

10000 PRINT "Please type in the name of the file you wish to access"
PRINT "or type Create File [number of file here] to start creating a file"
PRINT "Keep in mind that, once entered, files cannot be altered or removed"
PRINT "also any data you put in will be deleted when you close the session"
PRINT "In demontrative commands the square brackets just show what you are"
PRINT "supposed to put in that place"
PRINT "You can currantly make a max of five file(s)"
INPUT fileName$
IF fileName$ = "go to menu" THEN
    GOTO 2
END IF
IF fileName$ = "Create File 1" THEN
    GOTO 10025
END IF
IF fileName$ = "Create File 2" THEN
    GOTO 200
END IF
IF fileName$ = "Create File 3" THEN
    GOTO 300
END IF
IF fileName$ = "Create File 4" THEN
    GOTO 400
END IF
IF fileName$ = "Create File 5" THEN
    GOTO 500
END IF
IF fileName$ = fileNameOne$ THEN
    COLOR 6: PRINT DataFileOne$
    2000 COLOR 7: PRINT "Type continue to continue"
    INPUT continue$
    IF continue$ = "continue" THEN
        GOTO 1000000
    ELSE
        GOTO 2000
    END IF
END IF
IF fileName$ = fileNameTwo$ THEN
    COLOR 6: PRINT DataFileTwo$
    20 COLOR 7: PRINT "Type continue to continue"
    INPUT continue$
    IF continue$ = "continue" THEN
        GOTO 1000000
    ELSE
        GOTO 20
    END IF
END IF
IF fileName$ = fileNameThree$ THEN
    COLOR 6: PRINT DataFileThree$
    90000000000 COLOR 7: PRINT "Type continue to continue"
    INPUT continue$
    IF continue$ = "continue" THEN
        GOTO 1000000
    ELSE
        GOTO 90000000000
    END IF
END IF
IF fileName$ = fileNameFour$ THEN
    COLOR 6: PRINT DataFileFour$
    99009 COLOR 7: PRINT "Type continue to continue"
    INPUT continue$
    IF continue$ = "continue" THEN
        GOTO 1000000
    ELSE
        GOTO 99009
    END IF
END IF
IF fileName$ = fileNameFive$ THEN
    COLOR 6: PRINT DataFileFive$
    999 COLOR 7: PRINT "Type continue to continue"
    INPUT continue$
    IF continue$ = "continue" THEN
        GOTO 1000000
    ELSE
        GOTO 999
    END IF
END IF


1000000 PRINT "If what you tried to do did not work than"
PRINT "either the file name you have entered does not exist"
PRINT "or you typed the file name in wrong"
PRINT
GOTO 10000

'Create File 1
10025 PRINT "Anything you type in will be saved under whatever name you give it"
INPUT DataFileOne$
PRINT "What would you like to name this file?"
INPUT fileNameOne$
GOTO 10000

'Create File 2
200 PRINT "Anything you type in will be saved under whatever name you give it"
INPUT DataFileTwo$
PRINT "What would you like to name this file?"
INPUT fileNameTwo$
GOTO 10000

'Create File 3
300 PRINT "Anything you type in will be saved under whatever name you give it"
INPUT DataFileThree$
PRINT "What would you like to name this file?"
INPUT fileNameThree$
GOTO 10000

'Create File 4
400 PRINT "Anything you type in will be saved under whatever name you give it"
INPUT DataFileFour$
PRINT "What would you like to name this file?"
INPUT fileNameFour$
GOTO 10000

'Create File 5
500 PRINT "Anything you type in will be saved under whatever name you give it"
INPUT DataFileFive$
PRINT "What would you like to name this file?"
INPUT fileNameFive$
GOTO 10000

