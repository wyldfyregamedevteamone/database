DIM Num1 AS SINGLE
DIM Num2 AS SINGLE
DIM Sel AS STRING

CLS

121212 INPUT "Enter First Number: ", Num1
INPUT "Enter Second Number: ", Num2

COLOR 5: PRINT

PRINT "A) Add"
PRINT "S) Subtract"
PRINT "M) Multiply"
PRINT "D) Divide"
INPUT "Enter Selection: ", Sel

Sel = UCASE$(Sel)

PRINT

SELECT CASE Sel
    CASE "A"
        COLOR 2: PRINT "Answer:"; Num1 + Num2
    CASE "S"
        COLOR 2: PRINT "Answer:"; Num1 - Num2
    CASE "M"
        COLOR 2: PRINT "Answer:"; Num1 * Num2
    CASE "D"
        COLOR 2: PRINT "Answer:"; Num1 / Num2
END SELECT
COLOR 7:
INPUT "Would you like to exit? yes/no...", exit$
IF exit$ = "yes" THEN
    GOTO 2
END IF
IF exit$ = "no" THEN
    GOTO 121212
END IF
